from main.src import Admin

PATH_DATA_ADMIN = "../../resources/adminInfo.txt"
class Auth:

    def __init__(self, admin):
        self.admin = admin

    def isAuth(self):
        f = open(PATH_DATA_ADMIN, 'r')
        allAdmin = f.readlines()
        f.close()
        if len(allAdmin) > 0:
            for admin in allAdmin:
                currentAdmin = admin.strip('\n\t')
                currentAdmin = currentAdmin.split(';')
                if currentAdmin[0] == self.admin.getUsername() and currentAdmin[1] == self.admin.getPassword():
                    return True
            return False
