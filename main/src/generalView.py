from tkinter import *
from main.src import *
from tkinter import messagebox
import os
#from PIL import Image

PATH_ADMIN_INFO = "../../resources/adminInfo.txt"
userQues = BanckQuestions()
def loguin():
    global root
    global nameEL
    global passWordEL
    global error
    rootHome.destroy()
    root = Tk()
    root.minsize(240, 225)
    root.maxsize(250, 240)
    root.geometry("300x280+300+300")
    root.title('Loguin');
    introduction = Label(root, text='Please Loguin\n')
    introduction.grid(sticky=E)
    nameL = Label(root, text='Username: ')
    passWordL = Label(root, text='PassWord: ')
    error = Label(root)
    error.configure(text='')
    nameL.grid(row=1, sticky=W)
    passWordL.grid(row=2, sticky=W)
    nameEL = Entry(root)
    passWordEL = Entry(root)
    nameEL.grid(row=1, column=1, sticky=E)
    passWordEL.grid(row=2, column=1, sticky=E)
    loguinB = Button(root, text='Logued', command=checkLoguin)
    loguinB.grid(row=3, columnspan=2, pady=15)
    error.grid(row=4, columnspan=2, sticky=W)
    root.mainloop()

def addQuestion():
    global answer1E
    global answer2E
    global answer3E
    global answer4E
    global  questionE
    global answerRightE
    global answerError
    root.destroy()
    rootQ = Tk()
    rootQ.minsize(325, 250)
    rootQ.maxsize(300, 260)
    rootQ.geometry("300x280+300+300")
    rootQ.title('Add Questions')
    introduction = Label(rootQ, text='Add New Question and Possible Answers\n')
    introduction.grid(columnspan=2, sticky=E)
    questionL = Label(rootQ, text="Question: ")
    answer1 = Label(rootQ, text='Answer1: ')
    answer2 = Label(rootQ, text='Answer2: ')
    answer3 = Label(rootQ, text='Answer3: ')
    answer4 = Label(rootQ, text='Answer4: ')
    answerError = Label(rootQ)
    answerError.configure(text='')
    answerRight = Label(rootQ, text='Number\nAnswerRight: ')
    questionL.grid(row=1, sticky='W')
    answer1.grid(row=2, sticky='W')
    answer2.grid(row=3, sticky='W')
    answer3.grid(row=4, sticky='W')
    answer4.grid(row=5, sticky='W')
    answerRight.grid(row=6, sticky='W')
    questionE = Entry(rootQ)
    answer1E = Entry(rootQ)
    answer2E = Entry(rootQ)
    answer3E = Entry(rootQ)
    answer4E = Entry(rootQ)
    answerRightE = Entry(rootQ)
    questionE.grid(row=1, column=1, sticky=E)
    answer1E.grid(row=2, column=1, sticky=E)
    answer2E.grid(row=3, column=1, sticky=E)
    answer3E.grid(row=4, column=1, sticky=E)
    answer4E.grid(row=5, column=1, sticky=E)
    answerRightE.grid(row=6, column=1, sticky=E)
    submitB = Button(rootQ, text='Submit', command=saveQuestion)
    submitB.grid(row=7, columnspan=2, pady=5)
    answerError.grid(row=8, columnspan=2, sticky=W)
    rootQ.mainloop()

def startQuiz():
    global answerB1
    global answerB2
    global answerB3
    global answerB4
    global questionL
    global rootQ
    global contatore
    global banckQuestion
    global question
    global result
    rootHome.destroy()
    rootQ = Tk()
    contatore = 4
    result = 0
    banckQuestion = BanckQuestions();
    question = banckQuestion.generateQ()

    rootQ.minsize(240, 225)
    rootQ.maxsize(250, 240)
    rootQ.geometry("300x280+300+300")
    rootQ.title('TopQuiz')
    introduction = Label(rootQ, text='')
    introduction.pack()
    questionL = Label(rootQ, text=question.getQuestion())
    questionL.pack()
    answerB1 = Button(rootQ, bg='red', text=question.getAnswer()[0], command= lambda : giveAnswer(0))
    answerB1.pack()
    answerB2 = Button(rootQ, bg='yellow', text=question.getAnswer()[1], command=lambda:giveAnswer(1))
    answerB2.pack()
    answerB3 = Button(rootQ, bg='red', text=question.getAnswer()[2], command=lambda : giveAnswer(2))
    answerB3.pack()
    answerB4 = Button(rootQ, bg='yellow', text=question.getAnswer()[3], command=lambda : giveAnswer(3))
    answerB4.pack()
    rootQ.mainloop()

def home():
    global rootHome
    rootHome = Tk()
    rootHome.minsize(240, 225)
    rootHome.maxsize(250, 240)
    rootHome.title("Hello, Welcome")
    rootHome.geometry("300x280+300+300")
    introduction = Label(rootHome, text="You are ready to start\nLet's GO.")
    introduction.grid()
    img = PhotoImage(file='../../resources/newImg.png')
    panel = Label(rootHome, image = img)
    panel.grid(row=0, column=1, pady=15)
    motivation = Label(rootHome, text="if you think you can \ndo it. Then you'll get there, \nbut if you think the \nopposite. No need to try because \nyou never get there. The spirit \nguides the body.")
    motivation.grid(row=1, columnspan=2, sticky=E)
    restrictedarea = Button(rootHome, text='restricte area', command=loguin)
    restrictedarea.grid(row=2, column=0)
    start = Button(rootHome, text='Start', pady=5, command=startQuiz)
    start.grid(row=2, column=1, pady=5)
    #image = Image.open('../../resources/newIndex.png')
    #mage = image.resize((50, 40), Image.ANTIALIAS) #The (250, 250) is (height, width)
    #image.save('../../resources/newImg.png', 'png')
    #self.pw.pic = ImageTk.PhotoImage(image)
    rootHome.mainloop()

def goToHome(root):
    if root != None:
        root.destroy()
        home()

def giveAnswer(idButton):
    global question
    global contatore
    global result
    contatore -= 1
    id = int(idButton)
    if id == question.getIndex():
        result += 1
    question = banckQuestion.generateQ()
    questionL["text"] = question.getQuestion()
    answerB1["text"] = question.getAnswer()[0]
    answerB2["text"] = question.getAnswer()[1]
    answerB3["text"] = question.getAnswer()[2]
    answerB4["text"] = question.getAnswer()[3]
    if contatore <= 0:
        resultCallBack(result)
        rootQ.destroy()
        home()


def resultCallBack(result):
    msg = messagebox.showinfo("Result", "Your result is " + str(result) + "/4")

def saveQuestion():
    question = questionE.get()
    answerR1 = answer1E.get()
    answerR2 = answer2E.get()
    answerR3 = answer3E.get()
    answerR4 = answer4E.get()
    answerRight = answerRightE.get()
    if CheckData(question).formatString() and CheckData(answerR1).formatString() and CheckData(answerR2).formatString() \
            and CheckData(answerR3).formatString() and CheckData(answerR4).formatString() \
            and CheckData(answerRight).getIntFormat():
        Question(str(int(answerRight)-1), question, [answerR1, answerR2, answerR3, answerR4]).writeInDataCenter()
        answerError.configure(text='Succes add Question')
        questionE.delete(0, END)
        answer1E.delete(0, END)
        answer2E.delete(0, END)
        answer3E.delete(0, END)
        answer4E.delete(0, END)
        answerRightE.delete(0, END)
    else:
        answerError.configure(text='Error max length entry 25 and 0<= number <=3')


def checkLoguin():
    user = nameEL.get()
    userPass = passWordEL.get()
    admin = Admin(user, userPass);
    authAdmin = Auth(admin)
    if authAdmin.isAuth():
        addQuestion()
    if user == '' or userPass == '':
        error.configure(text='username or/and password is empty')
    if not authAdmin.isAuth():
        error.configure(text='username or password is wrong')

def main():
    home()

if __name__ == '__main__':
     main()
