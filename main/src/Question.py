
PATH_DATA_CENTER = "../../resources/dataCenterQuestion.txt"
class Question:
    def __init__(self, index, question, answer):
        self.index = index
        self.question = question
        self.answer = answer;

    def getIndex(self):
        return self.index

    def getQuestion(self):
        return self.question

    def getAnswer(self):
        return self.answer

    def writeInDataCenter(self):
        f = open(PATH_DATA_CENTER, "a")
        newStr = str(self.index) + ';'
        newStr = newStr + self.question + ';'
        for answer in self.answer:
            newStr = newStr + answer + ','
        newStr = newStr[:-1]
        f.write(newStr + "\n")
        f.close()
