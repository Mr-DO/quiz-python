
class CheckData:
    def __init__(self, data):
        self.data = data

    def getIntFormat(self):
        try:
            intFormat = int(self.data)
        except:
            return False
        return intFormat <=3 and intFormat >=0

    def formatString(self):
        return len(self.data) <= 25 and len(self.data) >=2

