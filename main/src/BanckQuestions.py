from main.src import *
import random
import os
PATH_DATA_CENTER = "../../resources/dataCenterQuestion.txt"
class BanckQuestions:
    def __init__(self):
        self.listQuestions = []
        self.path = PATH_DATA_CENTER;
        self.init()

    def generateQ(self):
        try:
            sizeBanck = len(self.listQuestions)
            return self.listQuestions[random.choice(list(range(sizeBanck)))]
        except:
            return "error DATA_CENTER"


    def isEmptyDateCenter(self):
        f = open(self.path, "r")
        if len(f.readlines()) <= 0:
            f.close()
            return True
        f.close()
        return False

    def init(self):
        f = open(self.path, "r")
        listQuestions = f.readlines()
        f.close()
        for elem in listQuestions:
            nextQuestion = elem.strip('\n\t')
            nextQuestion = nextQuestion.split(';')
            if len(nextQuestion) == 3:
                self.listQuestions.append(Question(int(nextQuestion[0]), nextQuestion[1], nextQuestion[2].split(',')))

def main():
    banck = BanckQuestions()
    print(banck.generateQ().question)

if __name__ == '__main__':
    main()
